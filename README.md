# Polybar Configuration

Polybar configuration (fizzy compliant)

## Documentation

See [wiki](https://gitlab.com/fizzycfg/official-cfg/configs-polybar/wikis/home)

## Contributing

See [CONTRIBUTING](./CONTRIBUTING.md)

## License

See [LICENSE](./LICENSE)