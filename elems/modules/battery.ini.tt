<%#                                                                          -%>
<%# File informations:                                                       -%>
<%# - Name:    elems/modules/battery.ini.tt                                  -%>
<%# - Summary: Polybar configuration - Battery module.                       -%>
<%# - Authors:                                                               -%>
<%#   - Alessandro Molari <molari.alessandro@gmail.com> (alem0lars)          -%>
<%#   - Luca Molari <molari.luca@gmail.com> (LMolr)                          -%>
<%#                                                                          -%>
<%# Project informations:                                                    -%>
<%#   - Homepage:        https://gitlab.com/alem0lars/configs-polybar        -%>
<%#   - Getting started: see README.md in the project root folder            -%>
<%#                                                                          -%>
<%# License: Apache v2.0                                                     -%>
<%#                                                                          -%>
<%# Licensed to the Apache Software Foundation (ASF) under one more          -%>
<%# contributor license agreements.  See the NOTICE file distributed with    -%>
<%# this work for additional information regarding copyright ownership.      -%>
<%# The ASF licenses this file to you under the Apache License, Version 2.0  -%>
<%# (the "License"); you may not use this file except in compliance with the -%>
<%# License.                                                                 -%>
<%# You may obtain a copy of the License at                                  -%>
<%#     http://www.apache.org/licenses/LICENSE-2.0                           -%>
<%# Unless required by applicable law or agreed to in writing, software      -%>
<%# distributed under the License is distributed on an "AS IS" BASIS,        -%>
<%# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied  -%>
<%# See the License for the specific language governing permissions and      -%>
<%# limitations under the License.                                           -%>
<%#                                                                          -%>
<%
  define_locals do
    prefixed('polybar.') do
      variable 'colors'
      variable 'colors.battery',  as: :bat_colors
      variable 'modules.battery', as: :bat
    end
  end
-%>
[module/battery]
type = internal/battery

; This is useful in case the battery never reports 100% charge
full-at = 99

; Use the following command to list batteries and adapters:
; $ ls -1 /sys/class/power_supply/
battery = <%= local!(:bat)[:battery_file] %>
adapter = <%= local!(:bat)[:ac_file] %>

; If an inotify event haven't been reported in this many
; seconds, manually poll for new values.
;
; Needed as a fallback for systems that don't report events
; on sysfs/procfs.
;
; Disable polling by setting the interval to 0.
;
; Default: 5
poll-interval = <%= local!(:bat)[:poll_interval] %>

; see "man date" for details on how to format the time string
; NOTE: if you want to use syntax tags here you need to use %%{...}
; Default: %H:%M:%S
time-format = %H:%M

; Format: Full.
; Available tags:
;   <label-full> (default)
;   <bar-capacity>
;   <ramp-capacity>
format-full = <label-full>

; Available tokens:
;   %percentage% (default)
label-full = "%{F<%= local!(:colors)[:icon] %>}%{T8}%{T-}%{F-}"

; Format: Charging.
; Available tags:
;   <label-charging> (default)
;   <bar-capacity>
;   <ramp-capacity>
;   <animation-charging>
format-charging = "<animation-charging> <label-charging>"

; Available tokens:
;   %percentage% (default)
;   %time%
;   %consumption% (shows current charge rate in watts)
label-charging = "%percentage%% (%time%)"

animation-charging-0 = "%{F<%= local!(:colors)[:icon] %>}%{T8}%{T-}%{F-}"
animation-charging-framerate = 750

; Format: Discharging.
; Available tags:
;   <label-discharging> (default)
;   <bar-capacity>
;   <ramp-capacity>
;   <animation-discharging>
format-discharging = "<ramp-capacity> <label-discharging>"

; Available tokens:
;   %percentage% (default)
;   %time%
;   %consumption% (shows current discharge rate in watts)
label-discharging = "%percentage%% (%time%)"

animation-discharging-0 = "%{T2}%{T-}"
animation-discharging-0-foreground = <%= local!(:bat_colors)[:discharging] %>
animation-discharging-1 = "%{T2}%{T-}"
animation-discharging-1-foreground = <%= local!(:colors)[:icon] %>
animation-discharging-framerate = 2000

; Capacity Ramp.
ramp-capacity-0 = "%{T8}%{T-}"
ramp-capacity-0-foreground = <%= local!(:bat_colors)[:empty] %>
ramp-capacity-1 = "%{T8}%{T-}"
ramp-capacity-1-foreground = <%= local!(:bat_colors)[:one_quarter] %>
ramp-capacity-2 = "%{T8}%{T-}"
ramp-capacity-2-foreground = <%= local!(:bat_colors)[:one_quarter] %>
ramp-capacity-3 = "%{T8}%{T-}"
ramp-capacity-3-foreground = <%= local!(:bat_colors)[:half] %>
ramp-capacity-4 = "%{T8}%{T-}"
ramp-capacity-4-foreground = <%= local!(:bat_colors)[:three_quarters] %>
ramp-capacity-5 = "%{T8}%{T-}"
ramp-capacity-5-foreground = <%= local!(:bat_colors)[:three_quarters] %>
ramp-capacity-6 = "%{T8}%{T-}"
ramp-capacity-6-foreground = <%= local!(:bat_colors)[:full] %>
