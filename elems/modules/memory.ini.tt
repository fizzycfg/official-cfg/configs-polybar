<%#                                                                          -%>
<%# File informations:                                                       -%>
<%# - Name:    elems/modules/memory.ini.tt                                   -%>
<%# - Summary: Polybar configuration - Memory module.                        -%>
<%# - Authors:                                                               -%>
<%#   - Alessandro Molari <molari.alessandro@gmail.com> (alem0lars)          -%>
<%#   - Luca Molari <molari.luca@gmail.com> (LMolr)                          -%>
<%#                                                                          -%>
<%# Project informations:                                                    -%>
<%#   - Homepage:        https://gitlab.com/alem0lars/configs-polybar        -%>
<%#   - Getting started: see README.md in the project root folder            -%>
<%#                                                                          -%>
<%# License: Apache v2.0                                                     -%>
<%#                                                                          -%>
<%# Licensed to the Apache Software Foundation (ASF) under one more          -%>
<%# contributor license agreements.  See the NOTICE file distributed with    -%>
<%# this work for additional information regarding copyright ownership.      -%>
<%# The ASF licenses this file to you under the Apache License, Version 2.0  -%>
<%# (the "License"); you may not use this file except in compliance with the -%>
<%# License.                                                                 -%>
<%# You may obtain a copy of the License at                                  -%>
<%#     http://www.apache.org/licenses/LICENSE-2.0                           -%>
<%# Unless required by applicable law or agreed to in writing, software      -%>
<%# distributed under the License is distributed on an "AS IS" BASIS,        -%>
<%# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied  -%>
<%# See the License for the specific language governing permissions and      -%>
<%# limitations under the License.                                           -%>
<%#                                                                          -%>
<%
  define_locals do
    prefixed('polybar.') do
      variable 'colors'
      variable 'colors.memory',  as: :mem_colors
      variable 'modules.memory', as: :mem
    end
  end
-%>
[module/memory]
type = internal/memory

; Poll interval.
interval = <%= local!(:mem)[:poll_interval] %>

; Available tags:
;   <label> (default)
;   <bar-used>
;   <bar-free>
;   <ramp-used>
;   <ramp-free>
;   <bar-swap-used>
;   <bar-swap-free>
;   <ramp-swap-used>
;   <ramp-swap-free>
format = "<ramp-used> <label>"

; Available tokens:
;   %percentage_used% (default)
;   %percentage_free%
;   %gb_used%
;   %gb_free%
;   %gb_total%
;   %mb_used%
;   %mb_free%
;   %mb_total%
;   %percentage_swap_used%
;   %percentage_swap_free%
;   %mb_swap_total%
;   %mb_swap_free%
;   %mb_swap_used%
;   %gb_swap_total%
;   %gb_swap_free%
;   %gb_swap_used%

label = "%gb_used%"

; Ramp Used.
ramp-used-0 = "%{T2}%{T-}"
ramp-used-0-foreground = <%= local!(:mem_colors)[:empty] %>
ramp-used-1 = "%{T2}%{T-}"
ramp-used-1-foreground = <%= local!(:mem_colors)[:one_quarter] %>
ramp-used-2 = "%{T2}%{T-}"
ramp-used-2-foreground = <%= local!(:mem_colors)[:half] %>
ramp-used-3 = "%{T2}%{T-}"
ramp-used-3-foreground = <%= local!(:mem_colors)[:three_quarters] %>
ramp-used-4 = "%{T2}%{T-}"
ramp-used-4-foreground = <%= local!(:mem_colors)[:full] %>
